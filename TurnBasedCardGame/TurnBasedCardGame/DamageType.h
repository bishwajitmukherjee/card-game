#pragma once

enum class DamageType 
{ 
	PHYSICAL, 
	MAGICAL
};
