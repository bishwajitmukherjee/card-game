#pragma once
#include "Unit.h"
#include <iostream>
#include "DamageType.h"
using std::string;

class Ionia
{
public:
	Ionia()
	{}
	~Ionia()
	{}
	Unit Light{ 50.f,5.f,5.f,15.f,DamageType::MAGICAL,"Greenglade Caretaker" };
	Unit Heavy{ 150.f,10.f,15.f,50.f,DamageType::MAGICAL,"Emerald Awakener" };
	Unit Ranged{ 60.f,5.f,5.f,40.f,DamageType::PHYSICAL,"Shadow Assassin" };
	Unit Special{ 400.f,25.f,25.f,70.f,DamageType::PHYSICAL,"YASUO" };

private:

};
