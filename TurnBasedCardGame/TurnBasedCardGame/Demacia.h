#pragma once
#include "Unit.h"
#include <iostream>
#include "DamageType.h"
using std::string;

class Demacia
{
public:
	Demacia()
	{}
	~Demacia()
	{}
	Unit Light{ 50.f,5.f,15.f,10.f,DamageType::PHYSICAL,"Demacian Soldier" };
	Unit Heavy{ 200,10.f,30.f,40.f,DamageType::PHYSICAL,"Brightsteel Protector" };
	Unit Ranged{ 100,5.f,15.f,20.f,DamageType::PHYSICAL,"Border Lookout" };
	Unit Special{ 500.f,20.f,50.f,50.f,DamageType::MAGICAL,"GAREN" };

private:

};



