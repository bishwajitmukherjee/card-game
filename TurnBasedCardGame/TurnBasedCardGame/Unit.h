#pragma once
#include "pch.h"
#include "HealthComponent.h"
#include "MathLib.h"
#include <iostream>
#include "DamageType.h"
#include <stdio.h>
#include<string>
using namespace std;

class Unit

{
public:
	Unit()
	{}
	~Unit()
	{}
	//unit variables
	HealthComponent hpComponent;
	DamageType _Damagetype;
	float Damage;

	//custom constructor for unit class to set the values
	Unit(float health, float armor, float magicResist,float damage,DamageType type,string name)
	{
		hpComponent = HealthComponent(health, armor, magicResist);
		Name = name;
		Damage = damage;
		_Damagetype = type;
	}
	//returns the Name of the unit
	string getUnitName()
	{
		return Name;
	}


private:
	
	string Name;
	
	


};
