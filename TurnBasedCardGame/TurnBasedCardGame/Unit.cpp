#include "pch.h"
#include "HealthComponent.h"
#include "MathLib.h"
#include "DamageType.h"

class Unit

{
public:
	Unit()
	{}
	~Unit()
	{}
	Unit(float health, float armor, float magicResist)
	{
		hpComponent = HealthComponent(health,armor,magicResist);
	}
private:
	HealthComponent hpComponent;


};
