// TurnBasedCardGame.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <cstdlib>
#include <iostream>
#include "Player.h"
#include <windows.h>
#include "AI.h"
#include "Singleton.h"
using namespace std;
HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);//to handle colors in console
class BoardManager :public Singleton<BoardManager>
{
public:
	Player* player1 = new Player();
	AI* ai = new AI();
	vector<Unit*> CardsOnBoardPlayer;
	vector<Unit*> CardsOnBoardAI;
	BoardManager()
	{}
	~BoardManager()
	{}

	
	//get input from player
	void GetPlayerInput()
	{
		cout << "Select Two Decks" << endl;
		int inp;
		for (int temp = 0; temp < 2; temp++)
		{
			
			cin >> inp;
			if (inp > 0 && inp <= 4)
			{
				player1->setArmy(inp);
			}
			else
			{
				cout << "Re-Enter input : " << endl;
				temp--;
			}

		}
	}


	//set AI
	void SetAI()
	{
		int inp;
		for (int temp = 0; temp < 2; temp++)
		{
			inp = (rand() % 4) + 1;
			ai->setArmy(inp);
		}
	}

	//function to set AI turns.
	void AITurn()
	{
		
	}

	//function to print the deck on your hand.
	void printUserDeck()
	{
		//prints your deck on hand.
		cout << "\n\nYour Deck : \n\n" << endl;
		cout << " UNITS        :";
		for (int i = 0; i < player1->Army.size(); i++)
		{
			string unitname = player1->Army[i].getUnitName();
			printf(" %d : %s ||", i + 1, unitname.c_str());
		}
		SetConsoleTextAttribute(handle, 2);
		cout << "\n HEALTH       : ";
		//for loop to print the health
		for (int i = 0; i < player1->Army.size(); i++)
		{
			string unitname = player1->Army[i].getUnitName();
			int nameLength = unitname.length() / 2;
			for (int j = 0; j < nameLength - 1; j++)
			{
				cout << " ";
			}
			float unithealth = player1->Army[i].hpComponent.getHealth();
			printf(" %0.2f ", unithealth);
			for (int j = 0; j < nameLength; j++)
			{
				cout << " ";
			}
			cout << "||";
		}

		//for loop to print armor
		SetConsoleTextAttribute(handle, 6);
		cout << "\n ARMOR        : ";
		for (int i = 0; i < player1->Army.size(); i++)
		{
			string unitname = player1->Army[i].getUnitName();
			int nameLength = unitname.length() / 2;
			for (int j = 0; j < nameLength - 1; j++)
			{
				cout << " ";
			}
			float unithealth = player1->Army[i].hpComponent.getArmor();
			printf(" %0.2f ", unithealth);
			for (int j = 0; j < nameLength; j++)
			{
				cout << " ";
			}
			cout << "||";
		}

		//for loop to print magic resist
		SetConsoleTextAttribute(handle, 3);
		cout << "\n MAGIC RESIST : ";
		for (int i = 0; i < player1->Army.size(); i++)
		{
			string unitname = player1->Army[i].getUnitName();
			int nameLength = unitname.length() / 2;
			for (int j = 0; j < nameLength - 1; j++)
			{
				cout << " ";
			}
			float unithealth = player1->Army[i].hpComponent.getMagicResist();
			printf(" %0.2f ", unithealth);
			for (int j = 0; j < nameLength; j++)
			{
				cout << " ";
			}
			cout << "||";
		}

		//for loop to print damage.
		SetConsoleTextAttribute(handle, 4);
		cout << "\n DAMAGE       : ";
		for (int i = 0; i < player1->Army.size(); i++)
		{
			string unitname = player1->Army[i].getUnitName();
			int nameLength = unitname.length() / 2;
			for (int j = 0; j < nameLength - 1; j++)
			{
				cout << " ";
			}
			float unithealth = player1->Army[i].Damage;
			printf(" %0.2f ", unithealth);
			for (int j = 0; j < nameLength; j++)
			{
				cout << " ";
			}
			cout << "||";
		}
		SetConsoleTextAttribute(handle, 7);
		cout << "\n=======================================================================================" << endl;
	}

	//function to print the AI Deck.
	void printAIDeck()
	{
		//prints AI deck on hand.
		cout << "\n\nAI Deck : \n\n" << endl;
		cout << " UNITS        :";
		for (int i = 0; i < ai->Army.size(); i++)
		{
			string unitname = ai->Army[i].getUnitName();
			printf(" %d : %s ||", i + 1, unitname.c_str());
		}

		SetConsoleTextAttribute(handle, 2);
		cout << "\n HEALTH       : ";
		for (int i = 0; i < ai->Army.size(); i++)
		{
			string unitname = ai->Army[i].getUnitName();
			int nameLength = unitname.length() / 2;
			for (int j = 0; j < nameLength - 1; j++)
			{
				cout << " ";
			}
			float unithealth = ai->Army[i].hpComponent.getHealth();
			printf(" %0.2f ", unithealth);
			for (int j = 0; j < nameLength; j++)
			{
				cout << " ";
			}
			cout << "||";
		}
		SetConsoleTextAttribute(handle, 6);

		cout << "\n ARMOR        : ";
		for (int i = 0; i < ai->Army.size(); i++)
		{
			string unitname = ai->Army[i].getUnitName();
			int nameLength = unitname.length() / 2;
			for (int j = 0; j < nameLength - 1; j++)
			{
				cout << " ";
			}
			float unitarmor = ai->Army[i].hpComponent.getArmor();
			printf(" %0.2f ", unitarmor);
			for (int j = 0; j < nameLength; j++)
			{
				cout << " ";
			}
			cout << "||";
		}
		SetConsoleTextAttribute(handle, 3);

		cout << "\n MAGIC RESIST : ";
		for (int i = 0; i < ai->Army.size(); i++)
		{
			string unitname = ai->Army[i].getUnitName();
			int nameLength = unitname.length() / 2;
			for (int j = 0; j < nameLength - 1; j++)
			{
				cout << " ";
			}
			float unitmagicresist = ai->Army[i].hpComponent.getMagicResist();
			printf(" %0.2f ", unitmagicresist);
			for (int j = 0; j < nameLength; j++)
			{
				cout << " ";
			}
			cout << "||";
		}

		SetConsoleTextAttribute(handle, 4);
		cout << "\n DAMAGE       : ";
		for (int i = 0; i < ai->Army.size(); i++)
		{
			string unitname = ai->Army[i].getUnitName();
			int nameLength = unitname.length() / 2;
			for (int j = 0; j < nameLength - 1; j++)
			{
				cout << " ";
			}
			float unitdamage = ai->Army[i].Damage;
			printf(" %0.2f ", unitdamage);
			for (int j = 0; j < nameLength; j++)
			{
				cout << " ";
			}
			cout << "||";
		}

		SetConsoleTextAttribute(handle, 7);
		cout << "\n=======================================================================================" << endl;

	}

	//function to do player turn.
	void PlayTurn()
	{
		//allows player to dray out two of his cards.
		CardsOnBoardPlayer.clear();
		CardsOnBoardAI.clear();
		cout << "\n Select 2 cards you want to play?" << endl;
		for (int i = 0; i < 2; i++)
		{

			int inp;
			cin >> inp;
			if ((inp > 0 && inp<9)&&player1->Army[inp - 1].hpComponent.IsAlive())
			{				
				CardsOnBoardPlayer.push_back(&player1->Army[inp - 1]);
			}
			else
			{
				cout << "Re-Enter Input";
				i--;
			}
			
		}
		int prevInp = -1;
		//select enemy AI cards.
		for (int i = 0; i < 2; i++)
		{
			int inp= (rand() % 8);
			while (!ai->Army[inp].hpComponent.IsAlive())
			{
				inp = (rand() % 8);
			}
			if (inp != prevInp)
			{
				CardsOnBoardAI.push_back(&ai->Army[inp]);
			}
			else
			{
				i--;
			}
		}

	}

	//function to make the Player Attack
	void PlayBoardPlayer(int CardNumber)
	{
		auto AttackingUnit = CardsOnBoardPlayer[CardNumber-1];
		auto damageType = AttackingUnit->_Damagetype;
		auto damageAmount = AttackingUnit->Damage;
		int attackIndex;
		cout << "Choose a unit the Card " << AttackingUnit->getUnitName() << " should attack : " ;
		cin >> attackIndex;
		while (attackIndex<1||attackIndex>2)
		{
			cout << "Re-Enter Target for " << AttackingUnit->getUnitName() << " : ";
			cin >> attackIndex;
		}
		cout << "\n===============================" << endl;
		float damageDealt = CardsOnBoardAI[attackIndex-1]->hpComponent.Damage(damageAmount, damageType);
		cout << "===============================" << endl;
		cout << CardsOnBoardPlayer[CardNumber-1]->getUnitName() << " dealt  " << damageDealt <<" to "<<CardsOnBoardAI[attackIndex - 1]->getUnitName() << endl;

	}

	//function to make the AI Attack
	void PlayBoardAI(int CardNumber)
	{
		auto AttackingUnit = CardsOnBoardAI[CardNumber-1];
		auto damageType = AttackingUnit->_Damagetype;
		auto damageAmount = AttackingUnit->Damage;
		int attackIndex=CardNumber;
		cout << "\n===============================" << endl;
		float damageDealt = CardsOnBoardPlayer[attackIndex - 1]->hpComponent.Damage(damageAmount, damageType);
		cout << "===============================" << endl;
		cout << CardsOnBoardAI[CardNumber - 1]->getUnitName() << " dealt  " << damageDealt << " to " << CardsOnBoardPlayer[attackIndex - 1]->getUnitName() << endl;
	}

	//function to print board cards.
	void printBoard()
	{
		cout << "Your Board : " << endl;
		for (int i = 0; i < CardsOnBoardPlayer.size(); i++)
		{
			string unitname = CardsOnBoardPlayer[i]->getUnitName();
			printf(" %d : %s ||", i + 1, unitname.c_str());
		}
		cout << "\n=======================================================================================" << endl;
		cout << "                       Health " << endl;
		cout << "=======================================================================================" << endl;
		for (int i = 0; i < CardsOnBoardPlayer.size(); i++)
		{
			string unitname = CardsOnBoardPlayer[i]->getUnitName();
			int nameLength = unitname.length() / 2;
			for (int j = 0; j < nameLength - 1; j++)
			{
				cout << " ";
			}
			float unithealth = CardsOnBoardPlayer[i]->hpComponent.getHealth();
			printf(" %0.2f ", unithealth);
			for (int j = 0; j < nameLength; j++)
			{
				cout << " ";
			}
			cout << "||";
		}
		cout << "\n=======================================================================================" << endl;

		//print AI Deck
		cout << "AI Board : " << endl;
		for (int i = 0; i < CardsOnBoardAI.size(); i++)
		{
			string unitname = CardsOnBoardAI[i]->getUnitName();
			printf(" %d : %s ||", i + 1, unitname.c_str());
		}
		cout << "\n=======================================================================================" << endl;
		cout << "                       Health " << endl;
		cout << "=======================================================================================" << endl;
		for (int i = 0; i < CardsOnBoardAI.size(); i++)
		{
			string unitname = CardsOnBoardAI[i]->getUnitName();
			int nameLength = unitname.length() / 2;
			for (int j = 0; j < nameLength - 1; j++)
			{
				cout << " ";
			}
			float unithealth = CardsOnBoardAI[i]->hpComponent.getHealth();
			printf(" %0.2f ", unithealth);
			for (int j = 0; j < nameLength; j++)
			{
				cout << " ";
			}
			cout << "||";
		}
		cout << "\n=======================================================================================" << endl;

	}
private:

};

BoardManager* Singleton<BoardManager>::mInstance = nullptr;

//function to print all decks.
void printAllDecks()
{
	
		Demacia* Dunits = new Demacia();
		SetConsoleTextAttribute(handle, 6);
		cout<<"============================================================================================"<<endl;
		cout << "                                           DEMACIA     " << endl;
		cout << "============================================================================================" << endl;
		cout << Dunits->Light.getUnitName() << "  ||  ";
		cout << Dunits->Ranged.getUnitName() << "  ||  ";
		cout << Dunits->Heavy.getUnitName() << "  ||  ";
		cout << Dunits->Special.getUnitName() << "  ||  ";
		delete Dunits;
	
		Noxus* Nunits = new Noxus();
		SetConsoleTextAttribute(handle, 4);
		cout << "\n============================================================================================" << endl;
		cout << "                                           NOXUS     " << endl;
		cout << "============================================================================================" << endl;
		cout << Nunits->Light.getUnitName() << "  ||  ";
		cout << Nunits->Ranged.getUnitName() << "  ||  ";
		cout << Nunits->Heavy.getUnitName() << "  ||  ";
		cout << Nunits->Special.getUnitName() << "  ||  ";
		delete Nunits;
	
		ShadowIsles* Sunits = new ShadowIsles();
		SetConsoleTextAttribute(handle, 3);
		cout << "\n============================================================================================" << endl;
		cout << "                                       SHADOW ISLES     " << endl;
		cout << "==============================================================================================" << endl;
		cout << Sunits->Light.getUnitName() << "  ||  ";
		cout << Sunits->Ranged.getUnitName() << "  ||  ";
		cout << Sunits->Heavy.getUnitName() << "  ||  ";
		cout << Sunits->Special.getUnitName() << "  ||  ";
		delete Sunits;
	
		Ionia* Iunits = new Ionia();
		SetConsoleTextAttribute(handle, 2);
		cout << "\n============================================================================================" << endl;
		cout << "                                           IONIA     " << endl;
		cout << "==============================================================================================" << endl;
		cout << Iunits->Light.getUnitName() << "  ||  ";
		cout << Iunits->Ranged.getUnitName() << "  ||  ";
		cout << Iunits->Heavy.getUnitName() << "  ||  ";
		cout << Iunits->Special.getUnitName() << "  ||  ";
		delete Iunits;
		cout << "\n==============================================================================================" << endl;
		SetConsoleTextAttribute(handle, 7);

}


int main()
{
	printAllDecks();


	//BoardManager* BM = new BoardManager();
	BoardManager* BM = BoardManager::Create();

	bool Win = false;
	BM->GetPlayerInput();
	BM->SetAI();

	
	//GameLoop.
	while (!Win)
	{
		int turnCounter = 1;
		BM->printUserDeck();
		BM->printAIDeck();
		BM->PlayTurn();
		BM->printBoard();
		cout << "					\n\n									PLAYER TURN          \n\n"<<endl;
		BM->PlayBoardPlayer(turnCounter);
		cout << "					\n\n									AI TURN      \n\n       " << endl;
		BM->PlayBoardAI(turnCounter);
		turnCounter++;
		BM->printBoard();
		cout << "					\n\n									PLAYER TURN          \n\n   " << endl;
		BM->PlayBoardPlayer(turnCounter);
		cout << "					\n\n									AI TURN      \n\n" << endl;
		BM->PlayBoardAI(turnCounter);	
		BM->printBoard();
		Sleep(10);
		system("CLS");
		if (!BM->player1->isArmyAlive())
		{
			cout << "==============================================================================================================================================";
			cout << ("										                  			AI WINS									");
			cout << "\n==============================================================================================================================================";
			Win = true;
		}
		if (!BM->ai->isArmyAlive())
		{
			cout << "==============================================================================================================================================";
			cout << ("												                          PLAYER WINS									");
			cout << "\n==============================================================================================================================================";
			Win = true;
		}
	}



	






	/*                TEST CASE
	Ionia Army2;
	auto AttackingUnit = Army2;
	auto damageType = AttackingUnit.Ranged._Damagetype;
	auto damageAmount = AttackingUnit.Ranged.Damage;
	cout << "damaging unit : " << AttackingUnit.Ranged.getUnitName()<<endl;
	cout << "===============================" << endl;
	cout << BM->player1->Army[2].getUnitName() << " : " << BM->player1->Army[2].hpComponent.getHealth() << endl;
	BM->player1->Army[2].hpComponent.Damage(damageAmount, damageType);
	cout << "===============================" << endl;
	cout << BM->player1->Army[2].getUnitName() << " : " << BM->player1->Army[2].hpComponent.getHealth() << endl;
	

	ShadowIsles Army3;
	auto AttackingUnit1 = Army3;
	auto damageType1 = AttackingUnit1.Light._Damagetype;
	auto damageAmount1 = AttackingUnit1.Light.Damage;
	cout << "damaging unit : " << AttackingUnit1.Light.getUnitName() << endl;
	cout << "===============================" << endl;
	cout << BM->player1->Army[2].getUnitName() << " : " << BM->player1->Army[2].hpComponent.getHealth() << endl;
	BM->player1->Army[2].hpComponent.Damage(damageAmount1, damageType1);
	cout << "===============================" << endl;
	cout << BM->player1->Army[2].getUnitName() << " : " << BM->player1->Army[2].hpComponent.getHealth() << endl;

	BM->printUserDeck();
	 BM->printAIDeck();

	*Demacia Army1;
	cout<<Army1.Heavy.getUnitName();
	Demacia Army2;
	auto AttackingUnit = Army2;
	auto damageType = AttackingUnit.Special._Damagetype;
	auto damageAmount = AttackingUnit.Special.Damage;
	Army1.Heavy.hpComponent.Damage(damageAmount,damageType);
	cout<<" Current health : "<<Army1.Heavy.hpComponent.getHealth();
	
	BM->printUserDeck();
	BM->printAIDeck();
	BM->PlayTurn();
	BM->printBoard();
	BM->PlayBoardPlayer(1);
	BM->PlayBoardPlayer(2);
	BM->PlayBoardAI(1);
	BM->PlayBoardAI(2);
	BM->printBoard();
	Sleep(5000);
	system("CLS");


	BM->printUserDeck();
	BM->printAIDeck();
	BM->PlayTurn();
	BM->printBoard();
	BM->PlayBoardPlayer(1);
	BM->PlayBoardPlayer(2);
	BM->PlayBoardAI(1);
	BM->PlayBoardAI(2);
	BM->printBoard();
	Sleep(5000);
	system("CLS");


	BM->printUserDeck();
	BM->printAIDeck();
	BM->PlayTurn();
	BM->printBoard();
	BM->PlayBoardPlayer(1);
	BM->PlayBoardPlayer(2);
	BM->PlayBoardAI(1);
	BM->PlayBoardAI(2);
	BM->printBoard();
	Sleep(5000);
	system("CLS"); */
	
}
