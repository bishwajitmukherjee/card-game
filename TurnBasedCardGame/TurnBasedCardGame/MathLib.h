#pragma once

	template <typename T>
	static T clamp(T value, T min, T max)//generic function to clamp values between a min and a max value.
	{
		if (value < min)return min;
		if (value > max)return max;
		return value;
	}


