#pragma once
#include "Demacia.h"
#include"Noxus.h"
#include "ShadowIsles.h"
#include "Ionia.h"
#include<string>
#include <vector> 
using namespace std;


class AI
{
public:
	AI()
	{}
	~AI()
	{}

	//sets up the players army.
	vector<Unit> Army;
	void setArmy(int input)
	{
		switch (input)
		{
		case 1:
		{
			Demacia* units = new Demacia();
			Army.push_back(units->Light);
			Army.push_back(units->Heavy);
			Army.push_back(units->Ranged);
			Army.push_back(units->Special);
			delete units;
			break;
		}
		case 2:
		{
			Noxus* units = new Noxus();
			Army.push_back(units->Light);
			Army.push_back(units->Heavy);
			Army.push_back(units->Ranged);
			Army.push_back(units->Special);
			delete units;
			break;
		}
		case 3:
		{
			ShadowIsles* units = new ShadowIsles();
			Army.push_back(units->Light);
			Army.push_back(units->Heavy);
			Army.push_back(units->Ranged);
			Army.push_back(units->Special);
			delete units;
			break;
		}
		case 4:
		{
			Ionia* units = new Ionia();
			Army.push_back(units->Light);
			Army.push_back(units->Heavy);
			Army.push_back(units->Ranged);
			Army.push_back(units->Special);
			delete units;
			break;
		}
		default:
			break;
		}
	}

	//function to check if all the Army is alive
	bool isArmyAlive()
	{
		for (int i = 0; i < Army.size(); i++)
		{
			if (Army[i].hpComponent.IsAlive())
			{
				return true;
			}
		}
		return false;
	}

};
