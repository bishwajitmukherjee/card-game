#pragma once
#include "MathLib.h"
#include "DamageType.h"
class HealthComponent
{
public:
	HealthComponent()
	{}
	~HealthComponent()
	{}
	HealthComponent(float health, float armor, float magicResist)
	{
		setHealth(health);
		setArmor(armor);
		setMagicResist(magicResist);
		setMaxHealth(health);
		isAlive = true;
	}
	float Damage(float damageAmount, DamageType type)
	{
		float damageDealtTotal=0;
		if (isAlive)
		{
			switch (type)
			{


			case DamageType::PHYSICAL:
			{
				float DamageDealt = getHealth() - (damageAmount - getArmor()/10);
				damageDealtTotal = (damageAmount - getArmor() / 10);
				setHealth(clamp(DamageDealt, 0.f, getMaxHealth()));
				break;
			}
			case DamageType::MAGICAL:
			{
				float DamageDealt = getHealth() - (damageAmount - getMagicResist()/10);
				damageDealtTotal = (damageAmount - getMagicResist() / 10);
				setHealth(clamp(DamageDealt, 0.f, getMaxHealth()));
				break;
			}
			default:
				break;
			}
			if (getHealth() <= 0)isAlive = false;
		}
		return damageDealtTotal;
	}
	
	//setters

	void setHealth(float health)
	{
		Health = health;
	}
	void setMaxHealth(float maxhealth)
	{
		MaxHealth = maxhealth;
	}
	void setArmor(float armor)
	{
		Armor = armor;
	}
	void setMagicResist(float mr)
	{
		MagicResist = mr;
	}


	//getters
	float getHealth()
	{
		return Health;
	}
	float getArmor()
	{
		return Armor;
	}
	float getMagicResist()
	{
		return MagicResist;
	}
	float getMaxHealth()
	{
		return MaxHealth;
	}

	bool IsAlive()
	{
		return isAlive;
	}


private:
	float MaxHealth;
	float Health;
	float Armor;
	float MagicResist;
	bool isAlive;
};

