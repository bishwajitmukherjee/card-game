#pragma once
#include "Unit.h"
#include <iostream>
#include "DamageType.h"
using std::string;

class Noxus
{
public:
	Noxus()
	{}
	~Noxus()
	{}
	Unit Light{ 50.f,5.f,5.f,15.f,DamageType::PHYSICAL,"Legion Rearguard" };
	Unit Heavy{ 180,10.f,15.f,50.f,DamageType::PHYSICAL,"Basilisk Rider" };
	Unit Ranged{ 80,5.f,5.f,25.f,DamageType::PHYSICAL,"Shariza The Blade" };
	Unit Special{ 300.f,25.f,25.f,100.f,DamageType::MAGICAL,"DRAVEN" };

private:

};
