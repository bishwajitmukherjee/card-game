#pragma once
#include "Unit.h"
#include <iostream>
#include "DamageType.h"
using std::string;

class ShadowIsles
{
public:
	ShadowIsles()
	{}
	~ShadowIsles()
	{}
	Unit Light{ 30.f,0.f,0.f,25.f,DamageType::MAGICAL,"Wraith Caller" };
	Unit Heavy{ 250,5.f,5.f,60.f,DamageType::MAGICAL,"Rhasa the Sunderer" };
	Unit Ranged{ 60.f,0.f,0.f,30.f,DamageType::MAGICAL,"Ethereal Remitter" };
	Unit Special{ 250.f,25.f,25.f,80.f,DamageType::PHYSICAL,"KALISTA" };

private:

};
