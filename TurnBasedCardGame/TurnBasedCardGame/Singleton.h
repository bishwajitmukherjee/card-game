#pragma once
template<typename T>
class Singleton
{
public:
	//making static create so that it will be same for all the instances
	static
		T* Create()
	{
		if (mInstance == nullptr)
		{
			mInstance = new T();
		}
		return mInstance;
	}

	static T* Destroy()
	{
		if (mInstance != nullptr)
		{
			delete mInstance;
			mInstance = nullptr;
		}
		return mInstance;
	}
	//function to get the instance
	static T* getInstance()
	{
		return mInstance;
	}
	//disallowing copy constructor or = assignment operator
	Singleton(const Singleton&) = delete;
	Singleton& operator= (const Singleton) = delete;





private:
	//static mInstance so that its initialized only once and not everytime an instance is being created
	static T* mInstance;
	//constructor

protected:
	//can be only called from child classes
	Singleton()
	{
	}
	//destructor
	virtual ~Singleton()
	{
	}

};